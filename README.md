i3-gaps and misc config files
=============================

# Introduction

Simple config for i3-gaps, polybar and dunst.

# Setup

setup.sh will make symlinks in users home directory / .config
directory to the files / directories of this repository.

## Dependencies

- i3-gaps (not vanilla i3)
- Dunst (for notifications)
- Polybar (Smart, simple and easy to config status bar)

# Uncommon keybindings

Using Vi inspired shortcuts for navigation (M-h for left, M-l for
right, M-j for down and M-k for up). Using similar commands for moving
windows, resizing etc.

Unfortunately this also means that I had to change splitting shortcuts
to M-g and M-v (split horizontal and vertical).

# Power

Poweroff-, suspend- and logout shortcuts uses systemd commands. Make
sure [polkit is installed](https://wiki.archlinux.org/index.php/allow_users_to_shutdown).

M-p opens a menu for choosing shutdown, reboot etc.