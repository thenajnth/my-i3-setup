#!/usr/bin/env python3

import re
import subprocess

ENC = 'utf-8'
XRANDR_PATH = 'xrandr'

DEVICES_CONFIG_MAP = (
    (['LVDS-1', 'HDMI-3'], [
        '--output', 'LVDS-1', '--off',
        '--output', 'HDMI-1', '--off',
        '--output', 'HDMI-2', '--off',
        '--output', 'HDMI-3', '--auto',
        '--output', 'VGA-1', '--off',
    ]),
    (['LVDS-1'], [
        '--output', 'LVDS-1', '--auto',
        '--output', 'HDMI-1', '--off',
        '--output', 'HDMI-2', '--off',
        '--output', 'HDMI-3', '--off',
        '--output', 'VGA-1', '--off',
    ])
)


def get_connected_devices():
    connected_devices = []
    output = subprocess.check_output([XRANDR_PATH]).decode(ENC)

    for line in output.splitlines():
        match = re.match(r'([A-Z]{1,4}-\d)\s(connected|disconnected)', line)
        if not match:
            continue
        device, status = match.group(1, 2)
        if status == 'connected':
            connected_devices.append(device)

    return connected_devices


def auto_set_screens(connected_devices):
    for device_config, xrandr_params in DEVICES_CONFIG_MAP:
        if set(device_config) == set(connected_devices):
            cmd = [XRANDR_PATH] + xrandr_params
            print('Runs: "{}"'.format(' '.join(cmd)))
            subprocess.call(cmd)
            break

if __name__ == '__main__':
    connected_devices = get_connected_devices()
    auto_set_screens(connected_devices)
