#!/usr/bin/env bash

set -e

# To know your ID just run "xinput list"
DEVICE_ID=11

# CODE
STRING=$(xinput list-props "$DEVICE_ID" |grep "Device Enabled")
IS_ENABLED=${STRING: -1}

if [ "$IS_ENABLED" -eq "1" ]; then
    xinput disable "$DEVICE_ID"
else
    xinput enable "$DEVICE_ID"
fi
