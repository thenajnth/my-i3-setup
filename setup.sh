#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"  # Get dir of this script

mkdir -p ~/.config  # Create .config-dir if not exist
mkdir -p ~/bin

OUTPUT_DIR="$HOME/.i3"
if [ ! -L "$OUTPUT_DIR" ]; then
    ln -s "$DIR/.i3" "$HOME"
else
    echo $OUTPUT_DIR already exist as file or symlink.
fi

OUTPUT_DIR="$HOME/.config/polybar"
if [ ! -L "$OUTPUT_DIR" ]; then
    ln -s "$DIR/.config/polybar" "$HOME/.config/"
else
    echo $OUTPUT_DIR already exist as file or symlink.
fi

OUTPUT_DIR="$HOME/.config/dunst"
if [ ! -L "$OUTPUT_DIR" ]; then
    ln -s "$DIR/.config/dunst" "$HOME/.config/"
else
    echo $OUTPUT_DIR already exist as file or symlink.
fi

OUTPUT_FILE="$HOME/bin/toggle_touchpad.sh"
if [ ! -L "$OUTPUT_FILE" ]; then
    ln -s "$DIR/bin/toggle_touchpad.sh" "$HOME/bin/"
else
    echo $OUTPUT_FILE already exist as file or symlink.
fi

